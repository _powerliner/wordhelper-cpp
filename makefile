CC = g++
CFLAGS= -Wall -Wextra -Wuninitialized -pedantic-errors -Wconversion

main:
	$(CC) $(CFLAGS) main.cpp -o wordhelper-cpp
