#pragma once
#include "WordList.h"
#include <string>
using namespace std;

class SearchSeq {
   private:
      string attempts[6];
      WordList list;

   public:
      SearchSeq() {
         list = WordList();
      }

      explicit SearchSeq(string fileName) {
         list = WordList(fileName);
      }

      bool getAttempt(string attempt);

      long unsigned int getListSize();

      void printList();
};

