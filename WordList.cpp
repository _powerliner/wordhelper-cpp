#include <iostream>
#include <fstream>
#include "WordList.h"

using namespace std;

bool WordList::readWordFile(const string fileName) {
   // Need to load the words from a file so that they can be used for comparison
   ifstream wordFile;
   string tempWord = "";

   wordFile.open(fileName);

   if(!wordFile) return false;

   while(wordFile >> tempWord) {
      // Make sure word is 5 characters long
      if (tempWord.length() == 5) {
         words.push_back(tempWord);
      }
      else {
         cout << "Found invalid word, ignoring..." << endl;
      }
   }

   return true;
}

long unsigned int WordList::getSize() {
   return words.size();
}

bool WordList::isEmpty() {
   return words.empty();
}

void WordList::printList() {
   for(long unsigned int i = 0; i < words.size(); i++) {
      cout << words.at(i) << endl;
   }
}

