#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <regex>
#include <fstream>

using namespace std;

class WordList {
   private:
      vector<string> words;

   public:
      WordList() {
         // Placeholder
      }

      explicit WordList(string fileName) {
         bool fileRead = this->readWordFile(fileName);
         if (!fileRead) {
            cout << "Error reading file" << endl;
            return;
         }
      }

      bool readWordFile(string fileName);

      long unsigned int getSize();

      bool isEmpty();

      void printList();
};

